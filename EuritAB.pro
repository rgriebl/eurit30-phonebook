#-------------------------------------------------
#
# Project created by QtCreator 2013-02-25T14:01:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EuritAB
TEMPLATE = app

INCLUDEPATH += \
    libvcard-1.0.2/include/vcard

DEPENDPATH += \
    libvcard-1.0.2/include/vcard

include(qextserialport-1.2rc/src/qextserialport.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    libvcard-1.0.2/libvcard/vcardproperty.cpp \
    libvcard-1.0.2/libvcard/vcardparam.cpp \
    libvcard-1.0.2/libvcard/vcard.cpp

HEADERS  += mainwindow.h \
    libvcard-1.0.2/include/vcard/vcardproperty.h \
    libvcard-1.0.2/include/vcard/vcardparam.h \
    libvcard-1.0.2/include/vcard/vcard.h \
    libvcard-1.0.2/include/vcard/libvcard_global.h

FORMS    +=
