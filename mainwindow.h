#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QTableWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);

private slots:
    void importFromEurit();
    void importFromVcf();
    bool clear();
    void exportToEurit();
    void load();
    void save();
    void fixupCell(int row, int col);
    void newEntry();
    void deleteEntry();
    void selectionChanged();

protected:
    void closeEvent(QCloseEvent *);
    
private:
    void fixupData();
    static QString escape(const QString &in, int col);

    QTableWidget *m_table;
    QAction *m_deleteAction;
};

#endif // MAINWINDOW_H
