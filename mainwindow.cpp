#include "mainwindow.h"
#include <QtGui>

#ifdef Q_OS_WIN
#  include <windows.h>
inline void xSleep(int ms) { Sleep(ms); }
#else
#  include <unistd.h>
inline void xSleep(int ms) { usleep(ms * 1000); }
#endif

#include "libvcard-1.0.2/include/vcard/vcard.h"

#include "qextserialenumerator.h"
#include "qextserialport.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenu *m = menuBar()->addMenu(tr("File"));
    m->addAction(tr("Load from File..."), this, SLOT(load()));
    m->addAction(tr("Save to File..."), this, SLOT(save()));
    m->addSeparator();
    m->addAction(tr("Import from vCard Files..."), this, SLOT(importFromVcf()));
    m->addAction(tr("Import from Eurit"), this, SLOT(importFromEurit()));
    m->addSeparator();
    m->addAction(tr("Export to Eurit"), this, SLOT(exportToEurit()));
    m->addSeparator();
    m->addAction(tr("Quit"), this, SLOT(close()), QKeySequence::Quit);

    m = menuBar()->addMenu(tr("Edit"));
    m->addAction(tr("New Entry"), this, SLOT(newEntry()), QKeySequence::InsertLineSeparator);
    m->addSeparator();
    m_deleteAction = m->addAction(tr("Delete Entry..."), this, SLOT(deleteEntry()), QKeySequence::Delete);
    m->addSeparator();
    m->addAction(tr("Clear All"), this, SLOT(clear()));

    m_table = new QTableWidget();
    m_table->setColumnCount(2);
    m_table->setHorizontalHeaderLabels(QStringList() << tr("Name") << tr("Number"));
    m_table->setSortingEnabled(true);
    m_table->setSelectionMode(QAbstractItemView::SingleSelection);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    connect(m_table, SIGNAL(cellChanged(int,int)), this, SLOT(fixupCell(int, int)));
    connect(m_table, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
    setCentralWidget(m_table);

    selectionChanged();
}

void MainWindow::closeEvent(QCloseEvent *ce)
{
    if (QMessageBox::question(this, windowTitle(), tr("Do you really want to quit?"), QMessageBox::Yes, QMessageBox::No | QMessageBox::Default) == QMessageBox::Yes) {
        ce->setAccepted(true);
    }
}

void MainWindow::selectionChanged()
{
    m_deleteAction->setEnabled(m_table->selectedItems().count() == 2);
}

void MainWindow::newEntry()
{
    int row = m_table->rowCount();
    m_table->setRowCount(row + 1);
    QTableWidgetItem *twi;
    m_table->setItem(row, 0, twi = new QTableWidgetItem(tr("<New>")));
    m_table->setItem(row, 1, new QTableWidgetItem());
    m_table->scrollToItem(twi);
    m_table->editItem(twi);
}

void MainWindow::deleteEntry()
{
    if (m_table->selectedItems().count() == 2) {
        if (QMessageBox::question(this, windowTitle(), tr("Do you really want to delete the selected entry?"), QMessageBox::Yes, QMessageBox::No | QMessageBox::Default) == QMessageBox::Yes) {
            m_table->removeRow(m_table->selectedItems().first()->row());
        }
    }
}

bool MainWindow::clear()
{
    if (QMessageBox::question(this, windowTitle(), tr("Do you really want to clear all entries?"), QMessageBox::Yes, QMessageBox::No | QMessageBox::Default) == QMessageBox::Yes) {
        m_table->setRowCount(0);
        selectionChanged();
        return true;
    }
    return false;
}

QString MainWindow::escape(const QString &in, int col)
{
    QMap<char, const char *> replacement;
    replacement.insert('\xe4', "ae");
    replacement.insert('\xf6', "oe");
    replacement.insert('\xfc', "ue");
    replacement.insert('\xc4', "Ae");
    replacement.insert('\xd6', "Oe");
    replacement.insert('\xdc', "Ue");
    replacement.insert('\xdf', "ss");
    replacement.insert(',', "");
    replacement.insert(';', "");

    QString out;
    for (int j = 0; j < in.length(); j++) {
        if (replacement.contains(in.at(j).toLatin1()))
            out.append(QLatin1String(replacement.value(in.at(j).toLatin1())));
        else
            out.append(in.at(j));
    }
    if (col == 0)
        return out.remove(QRegExp("[^A-Za-z0-9. -]"));
    else
        return out.remove(QRegExp("[^0-9]"));
}


void MainWindow::fixupData()
{
    for (int i = 0; i < m_table->rowCount(); ++i) {
        for (int j = 0; j < 2; ++j) {
            fixupCell(i, j);
        }
    }
}

void MainWindow::fixupCell(int row, int col)
{
    if (row < 0 || row >= m_table->rowCount() || col < 0 || col >= m_table->columnCount())
        return;

    QTableWidgetItem *twi = m_table->item(row, col);
    QString txt = twi->text();
    QString escTxt = escape(txt, col);

    if (txt != escTxt)
        twi->setText(escTxt);

    if (escTxt.length() > 20)
        twi->setBackground(QColor(255, 192, 192));
    else
        twi->setBackground(QBrush());
}



void MainWindow::importFromEurit()
{
    if (m_table->rowCount()) {
        if (!clear())
            return;
    }

    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

    QStringList portNames;
    foreach (QextPortInfo info, ports)
        portNames << info.friendName;

    bool ok = false;
    QString item = QInputDialog::getItem(this, windowTitle(), tr("Select serial port the Eurit is connected to:"), portNames, 0, false, &ok);

    if (!ok)
        return;

    int portIndex = -1;
    for (int i = 0; i < ports.count(); ++i) {
        if (item == ports.at(i).friendName) {
            portIndex = i;
            break;
        }
    }
    if (portIndex == -1)
        return;

    QScopedPointer<QextSerialPort> port(new QextSerialPort(ports.at(portIndex).portName, QextSerialPort::Polling));
    port->setBaudRate(BAUD2400);
    port->setFlowControl(FLOW_OFF);
    port->setDataBits(DATA_8);
    port->setParity(PAR_NONE);
    port->setStopBits(STOP_1);
    port->setTimeout(500);  // set timeouts to 500 ms

    if (!port->open(QIODevice::ReadWrite | QIODevice::Unbuffered)) {
        QMessageBox::warning(this, windowTitle(), tr("Could not open serial port %1:\n%2").arg(ports.at(portIndex).portName, port->errorString()), QMessageBox::Ok);
        return;
    }

    port->write("ATNG\r");
    QByteArray buffer;
    QElapsedTimer timer;
    timer.start();

    QProgressDialog progress(this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setLabelText(tr("Importing data from the Eurit phone..."));
    progress.setCancelButton(0);
    progress.setMinimumDuration(0);
    progress.setAutoClose(false);
    progress.setAutoReset(false);
    progress.setRange(0,0);
    progress.show();

    forever {
        int bytes = port->bytesAvailable();

        if (!bytes) {
            QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1000);
            xSleep(1000);

            if (timer.elapsed() > 5000)
                break;
        } else {
            buffer += port->read(bytes);
            timer.restart();
        }
    }
    port->close();

    if (!buffer.startsWith("OK\r\n")) {
        QMessageBox::warning(this, windowTitle(), tr("Error retrieving data from the Eurit phone."), QMessageBox::Ok);
        return;
    }
    QList<QByteArray> lines = buffer.mid(4).split('\n');
    foreach (const QByteArray &line, lines) {
        int pos = line.indexOf(';', 4);
        if (!line.startsWith("ATNS") || !line.endsWith('\r') || pos < 0)
            continue;
        QString name = QString::fromLatin1(line.mid(4, pos - 4).trimmed());
        QString number = QString::fromLatin1(line.mid(pos + 1).trimmed());

        int row = m_table->rowCount();
        m_table->setRowCount(row + 1);
        m_table->setItem(row, 0, new QTableWidgetItem(name));
        m_table->setItem(row, 1, new QTableWidgetItem(number));
    }
    fixupData();
    selectionChanged();
}

void MainWindow::exportToEurit()
{
    QList<QextPortInfo> ports = QextSerialEnumerator::getPorts();

    QStringList portNames;
    foreach (QextPortInfo info, ports)
        portNames << info.friendName;

    bool ok = false;
    QString item = QInputDialog::getItem(this, windowTitle(), tr("Select serial port the Eurit is connected to:"), portNames, 0, false, &ok);

    if (!ok)
        return;

    int portIndex = -1;
    for (int i = 0; i < ports.count(); ++i) {
        if (item == ports.at(i).friendName) {
            portIndex = i;
            break;
        }
    }
    if (portIndex == -1)
        return;

    QScopedPointer<QextSerialPort> port(new QextSerialPort(ports.at(portIndex).portName, QextSerialPort::Polling));
    port->setBaudRate(BAUD2400);
    port->setFlowControl(FLOW_OFF);
    port->setDataBits(DATA_8);
    port->setParity(PAR_NONE);
    port->setStopBits(STOP_1);
    port->setTimeout(500);  // set timeouts to 500 ms

    if (!port->open(QIODevice::ReadWrite | QIODevice::Unbuffered)) {
        QMessageBox::warning(this, windowTitle(), tr("Could not open serial port %1:\n%2").arg(ports.at(portIndex).portName, port->errorString()), QMessageBox::Ok);
        return;
    }

    QList<QByteArray> lines;
    lines.append("ATNC");

    for (int i = 0; i < m_table->rowCount(); ++i) {
        QString name = m_table->item(i, 0)->text();
        QMap<char, const char *> replacement;
        replacement.insert('\xe4', "ae");
        replacement.insert('\xf6', "oe");
        replacement.insert('\xfc', "ue");
        replacement.insert('\xc4', "Ae");
        replacement.insert('\xd6', "Oe");
        replacement.insert('\xdc', "Ue");
        replacement.insert('\xdf', "ss");
        replacement.insert(',', "");
        replacement.insert(';', "");

        QString modName;
        for (int j = 0; j < name.length(); j++) {
            if (replacement.contains(name.at(j).toLatin1()))
                modName.append(QLatin1String(replacement.value(name.at(j).toLatin1())));
            else
                modName.append(name.at(j));
        }

        QByteArray line = "ATNS";
        line += modName.toLatin1().left(20);
        line += ';';
        line += m_table->item(i, 1)->text().toLatin1().left(20);
        lines << line;
    }

    QProgressDialog progress(this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setLabelText(tr("Exporting data to the Eurit phone..."));
    progress.setCancelButton(0);
    progress.setMinimumDuration(0);
    progress.setRange(0, lines.count() + 1);
    progress.setValue(0);

    for (int i = 0; i < lines.count(); ++i) {
        port->write(lines.at(i) + "\r");

        QElapsedTimer timer;
        timer.start();
        QByteArray buffer;

        forever {
            int bytes = port->bytesAvailable();

            if (!bytes) {
                QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1000);
                xSleep(1000);

                if (timer.elapsed() > 5000)
                    break;
            } else {
                buffer += port->read(bytes);
                timer.restart();
            }
        }
        if (buffer != "OK\r\n") {
            progress.cancel();
            QMessageBox::warning(this, windowTitle(), tr("Could not export all data to the Eurit."), QMessageBox::Ok);
            return;
        }
        progress.setValue(i + 1);
    }
}

void MainWindow::load()
{
    if (m_table->rowCount()) {
        if (!clear())
            return;
    }
    static QString dir = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);

    QString file = QFileDialog::getOpenFileName(this, tr("Open EuritAB Files"), dir, tr("All files (*)"), 0, 0);

    if (!file.isEmpty())
        dir = QFileInfo(file).absolutePath();

    QFile f(file);
    if (f.open(QFile::ReadOnly)) {
        QDataStream ds(&f);
        int count;
        ds >> count;
        for (int i = 0; i < count; i++) {
            QString name, number;
            ds >> name >> number;

            int row = m_table->rowCount();
            m_table->setRowCount(row + 1);
            m_table->setItem(row, 0, new QTableWidgetItem(escape(name, 0)));
            m_table->setItem(row, 1, new QTableWidgetItem(escape(number, 1)));
        }
    } else {
        QMessageBox::warning(this, windowTitle(), tr("Could not open %1 for loading:\n%2").arg(file, f.errorString()), QMessageBox::Ok);
    }
    fixupData();
    selectionChanged();
}

void MainWindow::save()
{
    static QString dir = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);

    QString file = QFileDialog::getSaveFileName(this, tr("Save EuritAB Files"), dir, tr("All files (*)"), 0, 0);

    if (!file.isEmpty())
        dir = QFileInfo(file).absolutePath();

    QFile f(file);
    if (f.open(QFile::WriteOnly | QFile::Truncate)) {
        QDataStream ds(&f);
        int count = m_table->rowCount();
        ds << count;
        for (int i = 0; i < count; i++) {
            QString name = m_table->item(i, 0)->text();
            QString number = m_table->item(i, 1)->text();

            ds << name << number;
        }
        if (ds.status() != QDataStream::Ok) {
            QMessageBox::warning(this, windowTitle(), tr("Could not write all data to %1:\n%2").arg(file, f.errorString()), QMessageBox::Ok);
        }
    } else {
        QMessageBox::warning(this, windowTitle(), tr("Could not open %1 for saving:\n%2").arg(file, f.errorString()), QMessageBox::Ok);
    }
}

void MainWindow::importFromVcf()
{
    if (m_table->rowCount()) {
        if (!clear())
            return;
    }

    static QString dir = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);

    QStringList files = QFileDialog::getOpenFileNames(this, tr("Open vCard Files"), dir, tr("vCards (*.vcf);;All files (*)"), 0, 0);

    if (!files.isEmpty())
        dir = QFileInfo(files.first()).absolutePath();

    QList<vCard> cards;

    foreach (const QString &file, files)
        cards += vCard::fromFile(file);


    for (int i = 0; i < cards.size(); ++i) {
        vCard card = cards.at(i);
        QString baseName;

        if (card.contains(VC_FORMATTED_NAME)) {
            baseName = card.property(VC_FORMATTED_NAME).value();
        }

        if (baseName.isEmpty()) {
            QStringList nameParts = card.property(VC_NAME).values();
            QString firstName = nameParts.at(vCardProperty::Firstname).simplified();
            QString lastName = nameParts.at(vCardProperty::Lastname).simplified();

            if (!firstName.isEmpty() && !lastName.isEmpty())
                baseName =  lastName + " " + firstName;
            else if (firstName.isEmpty())
                baseName = lastName;
            else if (lastName.isEmpty())
                baseName = firstName;
        }
        if (baseName.isEmpty()) {
            baseName = card.property(VC_ORGANIZATION).values().join(QLatin1String(" ")).simplified();
        }
        if (baseName.isEmpty()) {
            continue;
        }

        for (int i = 0; i < card.properties().size(); ++i) {
            vCardProperty prop = card.properties().at(i);

            if (prop.name() == VC_TELEPHONE) {
                bool ignore = false;
                bool mobile = false;
                for (int i = 0; i < prop.params().size(); ++i) {
                    vCardParam param = prop.params().at(i);
                    if (param.value() == "FAX") {
                        ignore = true;
                        break;
                    } else if ((param.value() == "CELL") || (param.value() == "type=CELL")) {
                        mobile = true;
                    }
                }
                if (ignore)
                    continue;
                QString number = prop.value().simplified();
                if (number.startsWith('+'))
                    number = "00" + number.mid(1);

                number.remove(QRegExp("[^\\d]"));

                if (number.startsWith("0049"))
                    number = "0" + number.mid(4);
                if (number.startsWith("089"))
                    number = number.mid(3);

                QString name = baseName;
                if (mobile || number.startsWith("01"))
                    name.append(" H.");

                if (!number.isEmpty() && !name.isEmpty()) {
                    int row = m_table->rowCount();
                    m_table->setRowCount(row + 1);
                    m_table->setItem(row, 0, new QTableWidgetItem(escape(name, 0)));
                    m_table->setItem(row, 1, new QTableWidgetItem(escape(number, 1)));
                }
            }
        }
    }
    fixupData();
    selectionChanged();
}

