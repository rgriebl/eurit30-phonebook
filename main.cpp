#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication::setOrganizationDomain(QLatin1String("softforge.de"));
    QApplication::setOrganizationName(QLatin1String("Softforge"));
    QApplication::setApplicationName(QLatin1String("EuritAB"));

    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle(a.applicationName());
    w.show();
    
    return a.exec();
}
